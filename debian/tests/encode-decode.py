from __future__ import print_function
import platform
from pyeclib.ec_iface import ECDriver
import sys

# libisal2 is only available for amd64 and arm64 architectures
# Note: platform.machine() was returning 'x86_64' on an i386 machine so we
# also test sys.maxsize.
arch = platform.machine()
is64 = sys.maxsize > 2**32
if sys.argv[1].startswith('isa_') and (arch != 'x86_64' or not is64) and arch != 'aarch64':
    print("Skipping {} test for {} architecture, is64={}".format(sys.argv[1], arch, is64))
else:
    input = b'test'

    # Init
    print("init:", end=" ")
    ec = ECDriver(k=3, m=3, hd=3, ec_type=sys.argv[1])
    print("OK")

    # Encode
    print("encode:", end=" ")
    fragments = ec.encode(input)
    print("OK")

    # Decode
    print("decode:", end=" ")
    assert ec.decode(fragments[0:ec.k]) == input
    print("OK")
